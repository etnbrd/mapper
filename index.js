import React from 'react';
import ReactDOM from 'react-dom';

import List from './lib/list.js';
import Map from './lib/map.js';

require('./assets/index.scss');

var map = new Map("map");

ReactDOM.render(
  <List onItemAdd={map.addPlace}
        onItemRemove={map.removePlace}
        onValidatePreview={map.changeType}
        list={map.places}/>,
  document.getElementById('content')
);
