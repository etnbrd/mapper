var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: "./index.js",
  output: {
    path: __dirname + '/public',
    filename: "bundle.js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Mapper',
      template: 'assets/index.html'
    })
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader"
    }, {
      test: /\.scss$/,
      loaders: ['style-loader', 'css-loader', 'sass-loader']
    }, {
      test: /\.css$/,
      loaders: ['style-loader', 'css-loader']
    }, {
      test: /\.(jpe?g|png|gif)$/,
      loaders: ["url-loader"]
    },{
      test: /\.(eot|svg|ttf|woff|woff2)$/,
      loader: ['file-loader?name=[path][name].[ext]&context=assets'],
    }]
  }
};
