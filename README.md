I designed this small project as an assignement for an interview.

I spent 18h28 time on this, distributed like this :
- Creation and scaffholding : 28mn
- React and overall structure : 4h24
  <!-- - Autosuggest search bar -> 1h04
  - Add/remove cities, and update the bounding box -> 1h34
  - Remove button -> 21mn
  - Preview behavior -> ~1h10
  - Add behavior -> ~15mn -->
- D3 and leaflet : 5h09
  <!-- - First Map -> 36mn
  - d3 SVG layer implementation and markers -> 4h03
  - Labels -> 30mn -->
- Style : 3h20
  <!-- - Mapbox style -> 1h09
  - Sass -> 2h11 -->
- Refactoring, cleaning, and publishing : 3h03
  <!-- - Refactoring -> 1h06
  - Cleaning -> 1h39
  - Bundle code -> 18mn -->

## Assignement

The assignement was the following :

Create a web application consisting of :
- A Leaflet map
- A search bar
- A scrollable column list

The application should allow to build a list of cities, with their geographical representations as markers on the map.
So it should be possible to add and remove cities.
The search bar should use the Google maps api to autosuggest Cities
The markers should be on a d3 SVG layer, within the Leaflet map.
When the list change, the map should automatically update its view to fit all the cities.

---

Whil I was implementing this, I wrote my comments in the log file, beside.

## Design choices

I chose to integrate the search bar within the list, so that the user understand the relation between the two. Searching a city in the bar later allows to add it to the list.

I chose to preview the selected city before adding it to the list.
Once it is selected from the search bar, the user can validate it, to add it to the list to continue building the list, or clear the search bar to remove the preview.

Because the goal for the application was not very clear regarding the data to visualize, I went for the most simplistic markers : a circle. It represent well the fact that a city is not bound to a specific address, but spread over several miles.

## Limitations

Because I had a limited time, this application has some limitations that I am aware of.

*Mobile*
I completly discarded a responsive design. I worked on my laptop, and didn't wanted to test it on a smartphone.
Though, if I had to, I would have put the layout differently for mobile :
The list spanning full width at the bottom of the screen.
the search bar at the top of the list, to avoid scrolling to much, and keeping the map on screen while searching.

*React and D3*
These don't match so well.
D3 uses persistent list of data points, while react rebuilds the states of its component at every update.
Because of that, the design to connect the map and the list feels awkward and weak.

*Google map autosuggest*
Furthermore, because the google map autosuggest apis provide a ready to use dropdown to bind to an input, element, the design to connect it to the list is not idiomatic as well.

*Global design and Redux*
The implentation design was very drivven by the leaflet map and autosuggest input.
The project would have been bigger, and not so focuses on this list, I would have definitely gone for redux, to store the list of cities and provide it to the map, and to the list view. The search bar and list would then dispatch action of adding and removing a city.
