import React from 'react';
import GeoSearch from './geosearch';

class Item extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <li>
        <button className="remove"
                onClick={this.props.remove}>
          <i className='glyphicon-remove'/>
          <span className='label'>remove</span>
        </button>
        <span className='name'>{this.props.data.place.name}</span>
      </li>);
  }
}

// redux style, without redux :
// this.props.list points to the map, and onItemAdd points to map.addItem
// So when we add an item, it gets added to the map's list
// And because this component is rendered from props (not state),
// we need to re render it manually.
// That being said, it is a weak design, due to the fact that d3 needs
// persistent data arrays, while React drops them.

class List extends React.Component {
  constructor(props) {
    super(props);
  }

  addPreview(item) {
    this.props.onItemAdd(item, 'preview');
    this.forceUpdate();
  }

  removeItem(id) {
    this.props.onItemRemove(id);
    this.forceUpdate();
  }

  removePreview() {
    var p = this.props.list.find(p => p.type === 'preview')
    if (p) {
      this.removeItem(p.id);
    }
    this.forceUpdate();
  }

  validatePreview() {
    var p = this.props.list.find(p => p.type === 'preview')
    this.props.onValidatePreview(p.id, 'ok');
    this.forceUpdate();
  }

  render() {
    return (<div>
        <ul className='list'>
          { this.props.list.map(item => {
            return item.type !== 'preview' ? (<Item data={item} remove={() => this.removeItem(item.id)} key={item.id}/>) : '';
          }) }
          <li key="searchbox">
            <GeoSearch onSearch={this.addPreview.bind(this)} onClear={this.removePreview.bind(this)} onValidate={this.validatePreview.bind(this)}/>
          </li>
        </ul>
      </div>);
  }
}

export default List;
