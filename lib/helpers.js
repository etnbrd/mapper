import * as geo from 'd3-geo';

const zoomLevels = [
  360, // 0
  180, // 1
  90, // 2
  45, // 3
  22.5, // 4
  11.25, // 5
  5.625, // 6
  2.813, // 7
  1.406, // 8
  0.703, // 9
  0.352, // 10
  0.176, // 11
  0.088, // 12
  0.044, // 13
  0.022, // 14
  0.011, // 15
  0.005, // 16
  0.003, // 17
  0.001, // 18
  0.0005, // 19
];

function toGeoJSON(places) {
  return {
      "type": "FeatureCollection",
      "features": places.map(p => ({
        "type": "Feature",
        "geometry": {
          "type": "Point",
          "coordinates": p.coordinates
        },
        "properties": {
        }
      }))
  }
}

function getZoomFromBounds(topLeft, bottomRight) {
  const margin = 1.2;
  var deg = Math.max(
    (bottomRight[0] - topLeft[0]) * margin,
    (bottomRight[1] - topLeft[1]) * margin,
    0.05 // Default (minimum) bounding box degree
  );

  return zoomLevels.findIndex(l => l < deg);
}

function identityProjection(x, y) {
  return {x, y}
}

function boundingBox(projection = identityProjection) {
  // closure to keep projection in scope
  function streamProjectPoint(lat, lng) {
    var p = projection(lat, lng);
    this.stream.point(p.x, p.y);
  }

  var project = geo.geoPath().projection(geo.geoTransform({point: streamProjectPoint}));

  return function(places) {
    if (places.length === 0)
      return undefined;

    var box = project.bounds(toGeoJSON(places)),
        topLeft = box[0],
        bottomRight = box[1],
        zoom = getZoomFromBounds(topLeft, bottomRight),
        center = [
          topLeft[0] + (bottomRight[0] - topLeft[0]) / 2,
          topLeft[1] + (bottomRight[1] - topLeft[1]) / 2
        ];

    return { box, center, zoom }
  }
}

export default { boundingBox }
