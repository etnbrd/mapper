import Leaflet from 'leaflet';
import * as d3 from 'd3';
import React from 'react';
import helpers from './helpers';

require('../node_modules/leaflet/dist/leaflet.css');
const mapbox = "https://api.mapbox.com/styles/v1/etnbrd/cj02dh2dz003e2sof3c2wqydd/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZXRuYnJkIiwiYSI6ImNqMDJiajA4cTAwOWoycXI0ZHkyOHExNjkifQ.SHSV95Cv8jRJQ5FXVJ0Xuw"
var ID = 0;

class Map {
  constructor(id) {
    // Bind handlers
    this.geoToScreenProjection = this.geoToScreenProjection.bind(this);
    this.addPlace = this.addPlace.bind(this);
    this.removePlace = this.removePlace.bind(this);
    this.updateMarkers = this.updateMarkers.bind(this);
    this.changeType = this.changeType.bind(this);
    this.updateBoundingBox = this.updateBoundingBox.bind(this);

    // Initialize list of places
    this.places = [];

    // Initialize leaflet map
    this.map = Leaflet.map(id).setView([51.505, -0.09], 13); // Default to london, I kept it this way
    Leaflet.tileLayer(mapbox, {
        attribution: '<a href="https://etnbrd.com">etnbrd</a>'
    }).addTo(this.map);

    // Initialize d3 layer
    this.svg = d3.select(this.map.getPanes().overlayPane).append("svg");
    this.group = this.svg.append("g").attr("class", "leaflet-zoom-hide");

    // Initialize bounding box with projections
    this.geoBoundingBox = helpers.boundingBox();
    this.screenBoundingBox = helpers.boundingBox(this.geoToScreenProjection);

    this.map.on("zoom", this.updateMarkers);
    this.updateMarkers();
  }

  geoToScreenProjection(lat, lng) {
    return this.map.latLngToLayerPoint(new Leaflet.LatLng(lat, lng));
  }

  addPlace (place, type) {
    this.places.push({
      id: ID++,
      place,
      coordinates: [
        place.geometry.location.lat(),
        place.geometry.location.lng()
      ],
      type })
    this.updateBoundingBox();
  }

  removePlace (id) {
    this.places.splice(this.places.findIndex(p => p.id === id), 1);
    this.updateBoundingBox();
  }

  changeType (id, type) {
    var p = this.places.find(p => p.id === id)
    if (p) {
      p.type = type;
      this.updateMarkers();
    }
  }

  updateBoundingBox() {
    var bounds = this.geoBoundingBox(this.places);
    if (bounds) {
      this.map.setView(bounds.center, bounds.zoom);
    }
    this.updateMarkers();
  }

  updateMarkers() {
    const viewBox = 2000;
    const offset = viewBox / 2;

    const size = Math.pow((this.map.getZoom() + 1) * .4, 2.7);
    const textOffset = -size - 10;
    const fontSize = "20px";
    const strokeSize = size * .1;
    const strokeColor = "#F7005F";
    const dashArray = (d) => d.type === 'preview' ? size * .3 + ', ' + size * .18 : '0';
    const name = (d) => d.place.name;
    const translate = (d) => {
      var p = this.geoToScreenProjection(d.coordinates[0], d.coordinates[1]);
      return "translate(" + p.x + "," + p.y + ")";
    }

    var circles = this.group.selectAll(".circle")
      .data(this.places);
    var texts = this.group.selectAll(".name")
      .data(this.places);

    // Enter
    circles
      .enter()
      .append("circle")
      .attr("class", "circle")
      .style("fill", "transparent")
      .style("stroke", strokeColor)
      .style("stroke-width", strokeSize)
      .style("stroke-dasharray", dashArray)
      .attr("r", size)
      .attr("transform", translate);

    texts
      .enter()
      .append("text")
      .attr("class", "name")
      .attr("text-anchor", "middle")
      .text(name)
      .style("font-size", fontSize)
      .attr("transform", translate)
      .attr("y", textOffset);

    // Update
    circles
      .transition()
      .duration(0)
      .style("stroke-width", strokeSize)
      .style("stroke-dasharray", dashArray)
      .attr("r", size)
      .attr("transform", translate)
    texts
      .transition()
      .duration(0)
      .style("font-size", fontSize)
      .attr("transform", translate)
      .attr("y", textOffset);

    // Exit
    circles
      .exit()
      .remove()
    texts
      .exit()
      .remove()

    // Update view box only if there are places to compute the bounding box with
    if (this.places.length !== 0) {
      const bounds = this.screenBoundingBox(this.places);

      this.svg.attr("width", bounds.box[0][0] - bounds.box[0][0] + viewBox)
      .attr("height", bounds.box[0][1] - bounds.box[0][1] + viewBox)
      .style("left", bounds.box[0][0] - offset + "px")
      .style("top", bounds.box[0][1] - offset + "px");

      this.group.attr("transform", "translate(" + (-bounds.box[0][0] + offset) + "," + (-bounds.box[0][1] + offset) + ")");
    }
  }
}

export default Map;
