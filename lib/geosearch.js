import React from 'react';
import GoogleMapsLoader from 'google-maps';

const apikey = 'AIzaSyAt_hAOzpFdxfwDEWFBvvl05wXAZky48M8';

GoogleMapsLoader.KEY = apikey;
GoogleMapsLoader.LIBRARIES = ['places'];

class GeoSearch extends React.Component {
  constructor(props) {
    super(props);
    
    // Bind handlers
    this.selectInput = this.selectInput.bind(this);
    this.clearInput = this.clearInput.bind(this);
    this.validateInput = this.validateInput.bind(this);

    // Initializing state
    this.state = {
      remove: false,
      add: false
    }
  }

  componentDidMount() {
    GoogleMapsLoader.load((google) => {
      this.autocomplete = new google.maps.places.Autocomplete(this.input, { types: ['(cities)'] });
      this.autocomplete.addListener('place_changed', this.selectInput)
    });
  }

  selectInput() {
    this.props.onSearch(this.autocomplete.getPlace());
    this.setState({remove: true, add: true});
  }

  clearInput() {
    this.props.onClear();
    this.input.value = '';
    this.setState({remove: false, add: false});
  }

  validateInput() {
    this.props.onValidate();
    this.clearInput();
  }

  render() {
    return (
      <div className='geosearch'>
        <button className="remove"
                onClick={this.clearInput}
                disabled={!this.state.remove}>
          <i className='glyphicon-remove'/>
          <span className='label'>clear</span>
        </button>
        <div className='searchbox'>
          <input className='input'
                 ref={(ref) => this.input = ref}/>
        </div>
        <button className="validate"
                onClick={this.validateInput}
                disabled={!this.state.add}>
          <i className='glyphicon-ok'/>
          <span className='label'>validate</span>
        </button>
      </div>);
  }
}

export default GeoSearch
